package edu.weber.createchallengemicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CreateChallengeMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CreateChallengeMicroserviceApplication.class, args);
	}

}
